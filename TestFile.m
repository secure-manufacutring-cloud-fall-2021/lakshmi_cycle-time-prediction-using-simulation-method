syms h(t) p(t) Y t y
M = 10;
A = 0.01;
G = 10;
B = 1;

%   Linear Equations:
Lin_Eqn = [diff(h)==((A*G)/B)*(p-(M/A)); diff(p)==(-(B*M)/A)*(h-(G/B))];



%   Non Linear Equation:
NonLin_Eqn = [diff(h)==(-M*h)+(A*h*p); diff(p) == (G*p)-(B*h*p)];

[t, ylin] = sollin(Lin_Eqn);
[t, ynlin] = solnlin(NonLin_Eqn);

%   Plots
plot(ylin(:,2),ylin(:,1),'blue',ynlin(:,2),ynlin(:,1),'red');
title('Arpita V');
xlabel('Predator Population');
ylabel('Prey Population');
legend('Linear','Non-Linear','Location','eastoutside');

function [t, ylin] = sollin(Eqn)
    syms t Y
    M = 10;
    A = 0.01;
    G = 10;
    B = 1;
    [DEsys,subs] = odeToVectorField(Eqn);

    DEfunc = matlabFunction(DEsys, 'Vars',{t,Y});

    tspan = linspace(0,10,200);
    y0 = [1.2*(M/A) 1.2*(G/B)];
    [t,ylin] = ode45(DEfunc,tspan,y0);
end

function [t,ynlin] = solnlin(Eqn)
    syms t Y
    M = 10;
    A = 0.01;
    G = 10;
    B = 1;
    %   ODE45
    [DEsysnlin, subsnlin] = odeToVectorField(Eqn);

   
    DEfuncnlin = matlabFunction(DEsysnlin,'Vars',{t,Y});
   
    tspannlin = linspace(1,10,200);
    y0nlin= [1.2*(M/A) 1.2*(G/B)];
    [t,ynlin] = ode45(DEfuncnlin,tspannlin,y0nlin);
end